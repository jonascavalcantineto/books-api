FROM openjdk:8
LABEL MAINTAINER="Jonas Cavalcanti <jonascavalcantineto@gmail.com>"

ENV JAR_FILE="books-api-1.0.jar"

RUN useradd java
ADD package/${JAR_FILE} /
RUN chown java:java /${JAR_FILE}

USER java

CMD ["java", "-jar", "/books-api-1.0.jar"]
